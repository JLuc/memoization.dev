<?php
if (!defined('_CACHE_NAMESPACE'))
	define('_CACHE_NAMESPACE', $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].":");

if (!defined('_CACHE_KEY'))
	define('_CACHE_KEY', '0123456789ABCDEF');

class MCacheBackend_apc extends MCacheBackend {

	function key_for_lock($str) {
		return _CACHE_NAMESPACE . 'lock:' . md5($str . "locked");
	}

	function _xor($message) {
		$key = _CACHE_KEY;
		$keylen = strlen($key);
		if (!$keylen)
			return $message;

		$messagelen = strlen($message);
		for ($i = 0; $i < $messagelen; $i++) {
			$message[$i] = ~($message[$i] ^ $key[$i % $keylen]);
		}
		return $message;
	}

	function store_value($value) {
		if (is_object($value)
			or is_array($value)
			or is_bool($value)
			or !$value) {
				return array($this->_xor(serialize($value)));
		}
		return $this->_xor($value);
	}

	function unstore_value($a) {
		if (is_null($a)) {
			return $a;
		}
		// maybe_unserialize
		if (is_array($a)){
			return @unserialize($this->_xor($a[0]));
		}
		return $this->_xor($a);
	}

	function get($key) {
		$a = apc_fetch(_CACHE_NAMESPACE.$key);
		$return = $this->unstore_value($a);
		return $return;
	}
	
	function set($key, $value, $ttl=null) {
		// echo "<h4>Pour set($key, $value) avec "._CACHE_NAMESPACE." la valeur devient ".$this->store_value($value)."</h4>";
		// echo "Et si on unstore, ça donne ".$this->unstore_value($this->store_value($value))."<br><br>";

		return is_null($ttl)
			? apc_store(_CACHE_NAMESPACE.$key, $this->store_value($value))
			: apc_store(_CACHE_NAMESPACE.$key, $this->store_value($value), $ttl);
	}
	
	function exists($key) {
		return apc_exists(_CACHE_NAMESPACE.$key);
	}
	
	function del($key) {
		return apc_delete(_CACHE_NAMESPACE.$key);
	}
	
	function inc($key, $value=null, $ttl=null) {
		while (true) {
			if ($this->lock($key)) {
				$value = isset($value) ? intval($value) : 1;
				$value += intval($this->get($key));
				$this->set($key, $value, $ttl);
				$this->unlock($key);
				return $value;
			}
		} 
	}
	
	function dec($key, $value=null, $ttl=null) {
		$value = isset($value) ? intval($value) : 1;
		return $this->inc($key, -$value, $ttl);
	}

	function lock($key, /* private */ $unlock = false) {
	   return apc_add($this->key_for_lock($key), true);
	}
	
	function unlock($key) {
		return apc_delete($this->key_for_lock($key));
	}

	function size() {
		if (!class_exists('APCIterator'))
			return false;
		$a = new APCIterator('user', '/^'.preg_quote(_CACHE_NAMESPACE).'/', APC_ITER_ALL, 256*256);
		return $a->getTotalSize ();
	}

	function purge() {
		// le cache en memoire est de toute facon invalide par la globale cache_mark et le ttl est cense nettoyer le reste
		if (!class_exists('APCIterator'))
			return false;
		$a = new APCIterator('user', '/^'.preg_quote(_CACHE_NAMESPACE).'/', APC_ITER_ALL, 256*256);
		apc_delete($a);
		return true;
	}

	/**
	 * @param $action : commande 'new' pour commencer une itération, 'next' pour renvoyer la valeur suivante, 'rewind' pour revenir au début
	 * @param $params : options ou paramètres supplémentaires
	 *      regexp : regexp filtrant les caches (en plus du préfixe _CACHE_NAMESPACE). vide par défaut.
	 *      decode : indique si oui ou non on décode. true par défaut.
	 * @param int $n    : handle sur l'itération concernée. Si absent ou 0, c'est la dernière créée.
	 * @return array|int|void|null :
	 *      'new' renvoie le handle de l'itération créée
	 *      'next' renvoie un tableau (key => valeur), ou null s'il n'y a plus de valeurs
	 */
	function iterate($action, $params, $n=0) {

		if (!class_exists('APCIterator'))
			die ("La classe APCIterator n'est pas définie");

		static $a = array(), $nlast=0;
		if (!$n)
			$n = $nlast;
		if (($action!='new') and !$n)
			die ("Action $action sur itération non initialisée");

		// echo "APC iterate $action n=$n avec CACHE_NAMESPACE="._CACHE_NAMESPACE."<br>";

		switch($action) {
			case 'new':
				$reg = ((isset($params['regexp']))? $params['regexp'] : '');
				$reg = preg_quote(_CACHE_NAMESPACE).'.*'.preg_quote($reg);
				$a[++$nlast] = new APCIterator('user', '/^'.$reg.'/', APC_ITER_ALL, 256*256);
				if (!$a[$nlast])
					die ("Impossible de créer l'itérateur avec $reg");
				return $nlast;

			case 'get_key' :
			case 'get' :
			case 'val' :
			case 'next_key' :
			case 'next' :
				if (!($a[$n]->valid()))
					return null;

				if ((($action=='next_key') or ($action=='next')) and !$a[$n]->next())
					return null;

				if (($action=='get_key') or ($action == 'next_key')) {
					$return = short_key($a[$n]->key());
				}
				else {
					$cache = $a[$n]->current();
					if (!$cache)
						die ("Pas de cache courant dans l'itération");

					// par défaut, on décode la valeur
					// mais parfois on a pas besoin de la valeur alors pour optimiser on peut demander à ne pas décoder
					$decode = ((isset($params['decode'])) ? $params['decode'] : true);
					if ($cache and _CACHE_KEY and $decode) {
						$cache['value'] = $this->unstore_value($cache['value']);
					}

					$cache['key'] = short_key($cache['key']);

					$return = $cache;
					// tableau avec les clés key et value, communes à toutes les méthodes,
					// ainsi que des metas propres à APC : type, num_hits, mtime, creation_time, deletion_time
				}
				if (($action == 'get') or ($action == 'get_key'))
					$a[$n]->next();

				return $return;

			case 'count' :
				return $a[$nlast]->getTotalCount();

			default :
				die("mauvais parametre action '$action' pour iterate");
		}
	}

}

function short_key($k) {
	return substr($k, strlen(_CACHE_NAMESPACE));
}
