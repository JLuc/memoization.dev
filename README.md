# memoization.dev

Fork de https://plugins.spip.net/memoization.html pour dev tranquille

Permet de boucler sur les caches memoization en appliquant des actions aux caches satisfaisant une regexp.

Les actions dispo sont : `new` (pour créer l'itérateur) puis `get, get_key, val, next_key, next, count`

Voir
- [le source de iterate](https://gitlab.com/JLuc/memoization.dev/-/blob/82aecad36899c7e446e78040c97e163845ce4623/memo/apc.inc#L112)
- [la page wiki](https://gitlab.com/JLuc/memoization.dev/-/wikis/iterate) 

