<?php

/* Boucles SMARTDATA sur les caches mémoizés :
	on passe une regexp en argument pour filtrer les noms des caches
	Critères de la boucle pris en compte :
		- limit (cad {3,5})

<BOUCLE_smartdata(SMARTDATA){source memoization,test}{3,5}{datacache 0}>
	[#CLE] #VALEUR<br>
</BOUCLE_smartdata>

*/

function inc_memoization_to_smart_array_dist($criteres, $reg) {
	global $Memoization;
	if (!$Memoization) {
		die("Manque memoization");
	}

	$n = $Memoization->iterate('new', array('regexp'=>$reg));

	$count = 10000;
	if ($limit = $criteres['limit']) {
		list($start, $count) = explode(',', $limit);
		while ($start and ($Memoization->iterate('get_key'))) {
			$start--;
		}
		$criteres['limit'] = '';
	}
	$tableau = array();
	while ($count and ($key=$Memoization->iterate('get_key'))) {
		$tableau[] = $key;
		$count--;
	}
	return array ($tableau, $criteres);
}
