<?php

/* Boucles DATA sur les caches mémoizés :
	on passe une regexp en argument pour filtrer les noms des caches

<BOUCLE_smartdata(DATA){source memoization,test}{3,5}{datacache 0}>
	[#CLE] #VALEUR<br>
</BOUCLE_smartdata>

*/

function inc_memoization_to_array_dist($reg='') {
	global $Memoization;
	$return = array();
	if (!$Memoization) {
		die("Manque memoization");
	}
	$n = $Memoization->iterate('new', array('regexp'=>$reg));
	while ($key=$Memoization->iterate('get_key'))
		$return[]=$key;
	return $return;
}
